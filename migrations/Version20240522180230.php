<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240522180230 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket ADD events_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket ADD CONSTRAINT FK_97A0ADA39D6A1065 FOREIGN KEY (events_id) REFERENCES events (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_97A0ADA39D6A1065 ON ticket (events_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket DROP FOREIGN KEY FK_97A0ADA39D6A1065');
        $this->addSql('DROP INDEX UNIQ_97A0ADA39D6A1065 ON ticket');
        $this->addSql('ALTER TABLE ticket DROP events_id');
    }
}
