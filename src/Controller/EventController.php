<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\EventsRepository;

class EventController extends AbstractController
{
    #[Route('/event', name: 'app_event')]
    public function index(EventsRepository $eventsRepository): Response
    {
        $events = $eventsRepository->findAll();
        
        return $this->render('event/index.html.twig', [
            'events' => $events,
        ]);
    }

    

}
