<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AdminController extends AbstractController
{
    #[Route('/dashboard/admin', name: 'app_admin_dashboard')]
    public function index(): Response
    {
        return $this->render('dashboard/admin/index/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
}
