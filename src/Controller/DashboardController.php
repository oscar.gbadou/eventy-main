<?php

namespace App\Controller;
use App\Entity\Users;
use App\Form\ProfileImageType;
use App\Services\FileUploadService;
use App\Form\ChangePasswordFormType;
use App\Form\EditUserProfilFormType;
use App\Form\ChangeUserPasswordFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class DashboardController extends AbstractController
{
    #[Route('/dashboard/user', name: 'app_dashboard')]
    public function index(): Response
    {
        return $this->render('dashboard/user/index/index.html.twig', [
        ]);
    }


    #[Route('/dashboard/user/profil', name: 'app_dashboard_profil')]
    public function profil(): Response
    {

        $user = $this->getUser();     
        $form = $this->createForm(ProfileImageType::class);

        return $this->render('dashboard/user/profil/index.html.twig', [
            'ProfileImageForm' => $form->createView(),
        ]);
        
    }

    #[Route('/dashboard/user/profil/picture-image', name: 'app_dashboard_profil_image')]
    public function profilImage(Request $request,EntityManagerInterface $entityManager,FileUploadService $fileUploadService): Response
    {

        $user = $this->getUser();     
        $form = $this->createForm(ProfileImageType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('profilImage')->getData();
            if($file){
                   // Suppression de l'ancienne image si elle existe
                $oldFilePath = $this->getParameter('usersprofil_directory') . '/' . $user->getProfilImage();
               
                $fileUploadService->removeFile($oldFilePath); // Supprime l'ancienne image     
                  // Téléchargement du nouveau fichier et mise à jour de l'entité utilisateur         
                $newFileName =  $fileUploadService->uploadSingleImage(
                imageFile:$file,
                directory:$this->getParameter('usersprofil_directory'));
                 
                $user->setProfilImage($newFileName);
                $entityManager->flush();
                $this->addFlash('sucess','Votre photo de profil a été mis à jour avec succès');
   
                //redirection
                         // Récupération de l'URL de référence
                       $refererUrl = $request->headers->get('referer');

                     
                    // Redirection vers l'URL de référence ou une route par défaut si non disponible
                      return $this->redirect($refererUrl ?: $this->generateUrl('app_dashboard_profil'));                
              }
              
        }

        return $this->render('dashboard/user/profil/index.html.twig', [
            'ProfileImageForm' => $form->createView(),
        ]);
    }

    #[Route('/dashboard/user/profil/update', name: 'app_dashboard_profil_update')]
    public function profilUpdate(Request $request,EntityManagerInterface $entityManager): Response
    {
        $user =  $this->getUser();
        $form = $this->createForm(EditUserProfilFormType::class,$user);
        $form->handleRequest($request);
        $ProfileImageForm = $this->createForm(ProfileImageType::class);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager->flush();
            //rediriger avec message felase
           // return $this->redirectToRoute('app_dashboard_profil_update', [], Response::HTTP_SEE_OTHER);
           $this->addFlash('success',"Votre profil a été mis à jour avec succès");
           return $this->redirectToRoute('app_dashboard_profil_update');
           
        }
        
        return $this->render('dashboard/user/profil/update.html.twig',[
            'udapteUserProfilform'=>$form->createView(),
            'ProfileImageForm' => $ProfileImageForm->createView(),
        ]);
    }
    

    #[Route('/dashboard/user/profil/change-password', name: 'app_dashboard_profil_change_paswword')]
    public function changePassword(Request $request,EntityManagerInterface $entityManager,UserPasswordHasherInterface $passwordHasher ): Response
    {
        $user = $this->getUser() ;
        
        $form = $this->createForm(ChangeUserPasswordFormType::class);
        $form->handleRequest($request);
        $user = $this->getUser();
        if($form->isSubmitted() && $form->isValid()){
            $newPassword=  $form->get('plainPassword')->getData();
            $oldPassword =  $form->get('oldPassword')->getData();
                
            // Vérification de l'ancien mot de passe
            if(!$passwordHasher->isPasswordValid($user,$oldPassword)) {
                // Gérer l'erreur
                $this->addFlash('error', 'L\'ancien mot de passe est incorrect.');
                return $this->redirectToRoute('app_dashboard_profil_change_paswword');
            }
             // Encode(hash) the plain password, and set it.
             $encodedPassword = $passwordHasher->hashPassword(
                $user,
                $newPassword
            );

            $user->setPassword($encodedPassword);
            $entityManager->flush();
           // return $this->redirectToRoute('app_dashboard_profil_update', [], Response::HTTP_SEE_OTHER);
           $this->addFlash('success',"Votre mot de passe a été mis à jour avec succès");
           return $this->redirectToRoute('app_dashboard_profil_change_paswword');
           
        }
        
        return $this->render('dashboard/user/profil/changePassword.html.twig',[
            'changeUserPassword'=>$form->createView()
        ]);
    }


    
}
