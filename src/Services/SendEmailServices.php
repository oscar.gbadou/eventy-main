<?php 
namespace App\Services;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

    class SendEmailServices{
        public function __construct(private MailerInterface $mailer){}
        public function send(
                string $from,
                string $to,
                string $subject,
                string $templatee,
                Array $context= []
        ):void{
            $email= (new TemplatedEmail())
                 ->from(new Address($from))
                 ->to($to)
                 ->subject($subject)
                 ->htmlTemplate("emails/$templatee.html.twig")
                 ->context($context);
            $this->mailer->send($email);
        }
    }