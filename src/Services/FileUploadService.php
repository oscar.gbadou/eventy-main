<?php
namespace App\Services;

use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class FileUploadService
{
    public function __construct(private SluggerInterface $slugger){ }


    public function uploadSingleImage(
         object  $imageFile,
         string $directory
    )
    {
        $originalFilename = pathinfo($imageFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();
        // Move the file to the directory where brochures are stored
        try {
            $imageFile->move(
                $directory,
                $newFilename
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $newFilename;
    }


    public function removeFile(string $filePath)
    {
       try {
            if ($filePath && file_exists($filePath) && is_file($filePath)) {
                unlink($filePath); // Supprime le fichier
             
            }
       } catch(error){
            dump($error);
          
       }
    }

}