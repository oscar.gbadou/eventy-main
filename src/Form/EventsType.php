<?php

namespace App\Form;

use App\Entity\Events;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EventsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('name', TextType::class, [
            'label' => 'Nom de l\'évènement',
            'constraints' => [
                new Assert\NotBlank(),
                new Assert\Length(['max' => 255]),
            ],
        ])
        ->add('category', TextType::class, [
            'label' => 'Catégorie',
            'constraints' => [
                new Assert\NotBlank(),
            ],
        ])
        ->add('organizer', TextType::class, [
            'label' => 'Organisateur',
            'constraints' => [
                new Assert\NotBlank(),
            ],
        ])
        ->add('targetPublic', ChoiceType::class, [
            'label' => 'Public Cible',
            'choices' => [
                'Choississez une option'=>null,
                'Tout public' => 'Tout public',
                'Professionnel' => 'Professionnel',
                'Adulte' => 'Adulte',
                'Enfant' => 'Enfant'
            ],
            'constraints' => [
                new Assert\NotBlank(),
            ],
        ])

        ->add('numberOfGuests', IntegerType::class, [
            'label' => 'Nombre d\'invités ',
            'constraints' => [
                new Assert\NotBlank(),
            ],
        ])
        
        ->add('description', TextType::class, [
            'label' => 'Description (Speakers ,Services et commodités sur place ,Partenaires et sponsors)',
            'constraints' => [
                new Assert\NotBlank(),
            ],
        ])
        ->add('startDate', DateTimeType::class, [
            'label' => 'Date de Début',
            'widget' => 'single_text',
            'constraints' => [
                new Assert\NotBlank(),
          
            ],
        ])
        ->add('endDate', DateTimeType::class, [
            'label' => 'Date de Fin',
            'widget' => 'single_text',
            'constraints' => [
                new Assert\NotBlank(),
       
            ],
        ])
        ->add('image', FileType::class, [
            'label' => 'Image de l\'évènement',
            'data_class' => null,
            'required' => $options['image_required'],
            'mapped' => false,
            'constraints' => [
               
                new Assert\Image([
                    'maxSize' => '29M',
                ]),
            ],
        ])
        ->add('isVisible', ChoiceType::class, [
            'label' => 'Cet événement est actif ?',
            'choices' => [
                'Oui' => true,
                'Non' => false,
            ],
            'expanded' => true, // Renders as radio buttons
            'multiple' => false,
        ])
        
        ;
    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Events::class,
            'image_required' => false,
        ]);
    }
}