<?php

namespace App\Form;

use App\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Events;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('ticketNumber', IntegerType::class, [
            'label' => 'Nombre de Tickets à créer pour l\'évenement ',
            'constraints' => [
                new Assert\NotBlank([
                    'message' => 'Le nombre de tickets ne doit pas être vide.',
                ]),
                new Assert\Positive([
                    'message' => 'Le nombre de tickets doit être positif.',
                ]),
            ],
        ])
        ->add('ticketPrice', TextType::class, [
            'label' => 'Prix des Tickets',
            'constraints' => [
                new Assert\NotBlank([
                    'message' => 'Le prix du ticket ne doit pas être vide.',
                ]),
                
            ],
        ])
        ->add('events', EntityType::class, [
            'class' => Events::class,
            'choice_label' => 'name',
            'label' => 'Événement',
            'placeholder' => 'Sélectionnez un événement',
            'constraints' => [
                new Assert\NotBlank([
                    'message' => 'Veuillez sélectionner un événement.',
                ]),
            ],
        ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}