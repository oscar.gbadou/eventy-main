<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class EditUserProfilFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email',EmailType::class,[
                'label'=>'Votre email',
                'required'=>true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer votre adresse email.', //Assure que le champ n'est pas vide
                    ]),
                    new Email([
                        'mode' => 'strict',
                        'message' => 'Veuillez entrer une adresse email valide.', //Vérifie que l'email est valide
                    ]),
                    new Length([
                        'min' => 6,
                        'max' => 254,
                        'minMessage' => 'L\'email doit avoir au moins {{ limit }} caractères.',
                        'maxMessage' => 'L\'email ne peut pas dépasser {{ limit }} caractères.',
                    ]),
                ]
             ])
            ->add('firstName',TextType::class,[
                'label'=>'Votre Prénom',
                'required'=>false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer votre adresse prénom.', //Assure que le champ n'est pas vide
                    ]),
                    new Length([
                        'min' => 2,
                        'max' => 50,
                        'minMessage' => 'Le prénom doit contenir au moins {{ limit }} caractères',
                        'maxMessage' => 'Le prénom ne doit pas dépasser {{ limit }} caractères',
                    ]),
                ],
             ])
            ->add('lastName',TextType::class,[
                'label'=>'Votre Nom',
                'required'=>false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer votre adresse nom.', //Assure que le champ n'est pas vide
                    ]),
                    new Length([
                        'min' => 2,
                        'max' => 50,
                        'minMessage' => 'Le nom doit contenir au moins {{ limit }} caractères',
                        'maxMessage' => 'Le nom ne doit pas dépasser {{ limit }} caractères',
                    ]),
                ],
             ])
             ->add('phoneNumber', TextType::class, [
                'label' => 'Votre numéro de téléphone',
                'required' => false,
                'constraints' => [
                  /*  new NotBlank([
                        'message' => 'Veuillez entrer votre numéro de téléphone.',
                    ]),*/
                    new Regex([
                        'pattern' => '/^[0-9+\(\)#\.\s\/ext-]+$/',
                        'message' => 'Le numéro de téléphone est invalide.',
                    ]),
                    new Length([
                        'min' => 8,
                        'max' => 20,
                        'minMessage' => 'Le numéro de téléphone doit contenir au moins {{ limit }} chiffres',
                        'maxMessage' => 'Le numéro de téléphone ne doit pas dépasser {{ limit }} chiffres',
                    ]),
                ],
            ])
            ->add('country', ChoiceType::class, [
                    'label' => 'Votre pays',
                    'required' => false,
                    'placeholder' => 'Choisissez un pays',
                    'choices' => [
                        'France' => 'France',
                        'Allemagne' => 'Allemagne',
                        'Italie' => 'Italie',
                        'Espagne' => 'Espagne',
                        'Royaume-Uni' => 'Royaume-Uni',
                        'Portugal' => 'Portugal',
                        'Nigeria' => 'Nigeria',
                        'Ghana' => 'Ghana',
                        'Côte d\'Ivoire' => 'Côte d\'Ivoire',
                        'Sénégal' => 'Sénégal',
                        'Mali' => 'Mali',
                        'Burkina Faso' => 'Burkina Faso',
                        'Bénin' => 'Bénin',
                        'Togo' => 'Togo',
                    ],
                     
            
            ])
            
             ->add('submit', SubmitType::class, ['label' => 'Mettre à jour le profil']);

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
