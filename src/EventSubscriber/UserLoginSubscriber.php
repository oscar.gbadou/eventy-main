<?php

namespace App\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use App\Entity\Users;  


class UserLoginSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onLoginSuccessEvent(LoginSuccessEvent $event): void
    { 
        $user = $event->getPassport()->getUser();
        if ($user instanceof Users) { // Ensure it's an instance of your User entity
            $user->setLastLogin(new \DateTime()); // Update the last login date
            $this->entityManager->flush(); // Save the changes
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginSuccessEvent::class => 'onLoginSuccessEvent',
        ];
    }
}
