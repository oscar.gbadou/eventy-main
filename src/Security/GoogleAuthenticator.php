<?php

namespace App\Security;

// Importations nécessaires pour les différentes fonctionnalités utilisées
use App\Entity\Users;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use KnpU\OAuth2ClientBundle\Client\Provider\GoogleClient;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;





// Définition de la classe GoogleAuthenticator
class GoogleAuthenticator extends AbstractAuthenticator implements AuthenticationEntryPointInterface
{
    private $clientRegistry;
    private $entityManager;
    private $router;

    // Constructeur pour injecter les services nécessaires
    public function __construct(ClientRegistry $clientRegistry, EntityManagerInterface $entityManager, RouterInterface $router,UserPasswordHasherInterface $userPasswordHasher,LoggerInterface $logger)
    {
        $this->clientRegistry = $clientRegistry;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->userPasswordHasher = $userPasswordHasher; 
        $this->logger = $logger;


    }

    // Méthode pour déterminer si cet authenticator doit être utilisé pour la requête courante
    public function supports(Request $request): ?bool
    {
        // Retourne true si la route est celle définie pour le callback de Google OAuth
       // return $request->attributes->get('_route') === 'connect_google_check';

        $supports = $request->attributes->get('_route') === 'app_connect_google_check';
       
        // Utiliser le logger pour enregistrer les informations
        $this->logger->info('Support check for GoogleAuthenticator: ' . $supports);

        return $supports;
    }

    // Méthode pour authentifier l'utilisateur basé sur la requête
    public function authenticate(Request $request): Passport
    {
        // Récupération du client Google pour accéder aux services OAuth
        $client = $this->getGoogleClient();
        $accessToken = $client->getAccessToken();

        // Création d'un Passport en utilisant UserBadge, ce qui déclenche le chargement de l'utilisateur

                // Utilisation du token pour récupérer les données utilisateur de Google
                try {
                    $googleUser = $this->getGoogleClient()->fetchUserFromToken($accessToken);
                } catch (\Exception $e) {
                    // Log the error or handle it accordingly
                    $this->logger->error('Failed to fetch user from Google: ' . $e->getMessage());
                    throw new \Exception('Authentication with Google failed.');
                }
                $email = $googleUser->getEmail();
                // Recherche ou création d'un utilisateur en fonction de l'email
                $user = $this->entityManager->getRepository(Users::class)->findOneBy(['email' => $email]);
                if (!$user) {

                     // Création et enregistrement d'un nouvel utilisateur si non trouvé
                    $user = new Users();

                    $password = bin2hex(random_bytes(10));  // Générer un mot de passe aléatoire
                    $hashedPassword = $this->userPasswordHasher->hashPassword($user, $password);
                    
                    $user->setGoogleId($googleUser->getId());
                    $user->setEmail($googleUser->getEmail());
                    $user->setFirstName($googleUser->getFirstName());
                    $user->setLastName($googleUser->getLastName());
                    $user->setPassword($hashedPassword);

                    $this->entityManager->persist($user);
                    $this->entityManager->flush();

                }


                return new Passport(
                    new UserBadge($email, function ($email) {
                        return $this->entityManager->getRepository(Users::class)->findOneByEmail($email);
                    }),
                    new CustomCredentials(
                        function ($credentials, $user) {
                            // Vous pouvez implémenter une logique de vérification ici
                            return true;
                        },
                        $accessToken
                    )
                );
                
    }

    // Méthode auxiliaire pour obtenir le client Google
    private function getGoogleClient(): GoogleClient
    {
        return $this->clientRegistry->getClient('google');
    }

    // Gestion du succès de l'authentification
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // Redirection vers le tableau de bord après succès
        return new RedirectResponse($this->router->generate('app_dashboard'));
    }

    // Gestion de l'échec de l'authentification
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {

        // Enregistrement de l'erreur et redirection vers la page de connexion
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception->getMessage());
        return new RedirectResponse($this->router->generate('app_login'));
    }

    // Redirection des utilisateurs non authentifiés
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new RedirectResponse($this->router->generate('app_login'));
    }
}
