<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TicketRepository::class)]
class Ticket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\Column]
    private ?int $ticketNumber = null;

    #[ORM\Column(length: 255)]
    private ?string $ticketPrice = null;

    #[ORM\OneToOne(inversedBy: 'ticket', cascade: ['persist', 'remove'])]
    private ?Events $events = null;

    #[ORM\Column(length: 255)]
    private ?string $ticketUniqueId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

   


    public function getTicketNumber(): ?int
    {
        return $this->ticketNumber;
    }

    public function setTicketNumber(int $ticketNumber): static
    {
        $this->ticketNumber = $ticketNumber;

        return $this;
    }

    public function getTicketPrice(): ?string
    {
        return $this->ticketPrice;
    }

    public function setTicketPrice(string $ticketPrice): static
    {
        $this->ticketPrice = $ticketPrice;

        return $this;
    }

    public function getEvents(): ?Events
    {
        return $this->events;
    }

    public function setEvents(?Events $events): static
    {
        $this->events = $events;

        return $this;
    }

    public function getTicketUniqueId(): ?string
    {
        return $this->ticketUniqueId;
    }

    public function setTicketUniqueId(string $ticketUniqueId): static
    {
        $this->ticketUniqueId = $ticketUniqueId;

        return $this;
    }
}